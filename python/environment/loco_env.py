# TODO LIST
# - add recommended speed to the track (utility for close to that speed and not to fast)

import random
import numpy as np

GAME_OVER_REWARD = -1000.0

MIN_TRACK_DISTANCE = 500
MAX_TRACK_DISTANCE = 1000
AVG_TIME_PER_DISTANCE_UNIT = 0.1  # TODO or how to come up with a better estimate the planned arrival time

MAX_NUMBER_OF_WAGONS = 5
TIME_DELTA_PER_STEP = 1


class LocoEnv(object):
    """
    A very simple environment model, which interfaces are inspired by OpenAI gym:
    Read more: https://gym.openai.com/docs/
    """

    OBS_VALUES = 8
    OBS_IDX_SPEED = 0
    OBS_IDX_ENERGY = 1
    OBS_IDX_ENERGY_TOTAL = 2
    OBS_IDX_TOTAL_REMAINING_DISTANCE = 3
    OBS_IDX_TOTAL_REMAINING_DISTANCE_RELATIVE = 4
    OBS_IDX_NEXT_EVENT_REMAINING_DISTANCE = 5
    OBS_IDX_TOTAL_REMAINING_TIME = 6
    OBS_IDX_TOTAL_REMAINING_TIME_RELATIVE = 7

    def __init__(self):
        self.total_track_distance = 0
        self.planned_arrival_time = 0
        self.current_time = 0

        self.loco = None
        self.stopEvents = []
        self.observation = None
        pass

    def reset(self):
        self.total_track_distance = MIN_TRACK_DISTANCE + (MAX_TRACK_DISTANCE - MIN_TRACK_DISTANCE) * random.random()
        self.planned_arrival_time = self.total_track_distance * AVG_TIME_PER_DISTANCE_UNIT + random.normalvariate(0, 1)
        self.current_time = 0

        rnd_number_of_wagons = random.randint(0, MAX_NUMBER_OF_WAGONS)
        self.loco = Locomotive(rnd_number_of_wagons)

        self.stopEvents = self._create_random_events(self.total_track_distance, self.planned_arrival_time)

        self.observation = np.zeros((LocoEnv.OBS_VALUES,), dtype=np.float32)

        self.update_observation()

        return self.observation

    def _get_next_event_ahead(self):
        next_event = None
        min_location = float('inf')  # smaller location value is always closer to train
        for event in self.stopEvents:
            if event.is_visible and (event.location_on_track >= self.loco.traveled_distance):
                if event.location_on_track < min_location:
                    next_event = event
                    min_location = event.location_on_track

        return next_event

    def _create_random_events(self, total_distance, total_time):
        events = []

        rnd_num_event = random.randint(StopEvent.MIN_NUMBER_OF_UNEXPECTED_EVENTS, StopEvent.MAX_NUMBER_OF_UNEXPECTED_EVENTS + 1)
        MAX_ATTEMPTS = 3 * StopEvent.MAX_NUMBER_OF_UNEXPECTED_EVENTS
        attempts = 0
        while len(events) < rnd_num_event and attempts < MAX_ATTEMPTS:
            attempts += 1
            rnd_location = StopEvent.MIN_DISTANCE_TO_EDGES + random.random() * (total_distance - StopEvent.MIN_DISTANCE_TO_EDGES * 2)
            rnd_trigger_time = random.random() * total_time

            min_delta_dist = float('inf')
            for e in events:
                delta_dist = abs(e.location_on_track - rnd_location)
                min_delta_dist = min(min_delta_dist, delta_dist)

            if min_delta_dist > StopEvent.MIN_DISTANCE_BETWEEN_EVENTS:
                events.append(StopEvent(rnd_trigger_time, rnd_location))

        return events

    def step(self, action):

        self.current_time += TIME_DELTA_PER_STEP

        if len(self.stopEvents) > 0:
            for event in self.stopEvents:
                event.update(self.current_time, self.loco)

        energy_consumed, loco_distance_delta = self.loco.update(TIME_DELTA_PER_STEP, action)

        self.update_observation(energy_consumed)

        # calculate the reward for the previous action
        reward = 0

        if self.current_time <= self.planned_arrival_time:
            reward += 0.000001 * loco_distance_delta
        else:
            reward -= 0.00005 * TIME_DELTA_PER_STEP

        done = False

        # check crash with unexpected event
        if len(self.stopEvents) > 0:
            for event in self.stopEvents:
                if event.check_train_crashing(self.loco):
                    done = True
                    reward = GAME_OVER_REWARD
                    break

        # check goal reached
        if self.loco.traveled_distance >= self.total_track_distance:
            done = True

            if self.loco.current_speed > 10:
                reward = GAME_OVER_REWARD

        return self.observation, reward, done

    def update_observation(self, energy_consumed=0):
        self.observation[LocoEnv.OBS_IDX_SPEED] = self.loco.current_speed
        self.observation[LocoEnv.OBS_IDX_ENERGY] = energy_consumed
        self.observation[LocoEnv.OBS_IDX_ENERGY_TOTAL] = self.loco.total_energy_consumed
        self.observation[
            LocoEnv.OBS_IDX_TOTAL_REMAINING_DISTANCE] = self.total_track_distance - self.loco.traveled_distance
        next_event = self._get_next_event_ahead()
        self.observation[LocoEnv.OBS_IDX_NEXT_EVENT_REMAINING_DISTANCE] = -1 if next_event is None else (
                    next_event.location_on_track - self.loco.traveled_distance)
        self.observation[LocoEnv.OBS_IDX_TOTAL_REMAINING_DISTANCE_RELATIVE] = 1.0 - (
                    self.loco.traveled_distance / self.total_track_distance)
        self.observation[LocoEnv.OBS_IDX_TOTAL_REMAINING_TIME] = self.planned_arrival_time - self.current_time
        self.observation[LocoEnv.OBS_IDX_TOTAL_REMAINING_TIME_RELATIVE] = 1.0 - (
                    self.current_time / self.planned_arrival_time)

    @property
    def observation_space(self):
        return np.zeros((LocoEnv.OBS_VALUES,), dtype=np.float32)

    def print_info(self):
        print('Planned arrival time:         {}'.format(self.planned_arrival_time))
        print('Total track distance:         {}'.format(self.total_track_distance))
        print('Loco > total energy consumed: {}'.format(self.loco.total_energy_consumed))
        print('Loco > current speed:         {}'.format(self.loco.current_speed))
        print('Events > count:               {}'.format(len(self.stopEvents)))

    def print_status(self):
        LENGTH = 160
        track = list(' ' + LENGTH * '-' + ' ')
        progress_index = int(LENGTH * (1 - self.observation[LocoEnv.OBS_IDX_TOTAL_REMAINING_DISTANCE_RELATIVE]))
        track[progress_index] = 'T'

        for stopEvent in self.stopEvents:
            stop_event_index = int(LENGTH * (stopEvent.location_on_track / self.total_track_distance))
            if stopEvent.is_visible:
                track[stop_event_index] = 'V'
            if stopEvent.is_blocking:
                track[stop_event_index] = '!'
            if stopEvent.is_okay:
                track[stop_event_index] = 'G'

        print('{:02d}: [{}]'.format(self.current_time, ''.join(track)))


class Locomotive(object):
    MASS_OF_MAIN_UNIT = 15
    MASS_OF_SECONDARY_UNIT = 10

    LENGTH_OF_MAIN_UNIT = 0.015
    LENGTH_OF_SECONDARY_UNIT = 0.01

    def __init__(self, number_of_units):
        self.traveled_distance = 0
        self.current_speed = 0
        self.current_acceleration = 0
        self.number_of_units = number_of_units
        self.total_energy_consumed = 0

    def update(self, time_delta, action):
        """
        final_speed = initial_speed + a * dt
        final_distance = initial_speed * dt + 1/2 * a * dt^2
        """
        K = 0.25
        E = 0.15
        self.current_acceleration = K * action
        prev_speed = self.current_speed
        self.current_speed = prev_speed + self.current_acceleration * time_delta
        distance_delta = prev_speed * time_delta + 0.5 * self.current_acceleration * (time_delta) ** 2
        self.traveled_distance += distance_delta

        energy_consumed = E * ((self.mass * self.current_speed ** 2) - (self.mass * prev_speed ** 2)) / time_delta
        self.total_energy_consumed += energy_consumed
        return energy_consumed, distance_delta

    def estimate_ideal_total_power_for_track(self, track_distance, planned_time):
        return 0  # TODO

    @property
    def mass(self):
        return Locomotive.MASS_OF_MAIN_UNIT + self.number_of_units * Locomotive.MASS_OF_SECONDARY_UNIT

    @property
    def length(self):
        return Locomotive.LENGTH_OF_MAIN_UNIT + self.number_of_units * Locomotive.LENGTH_OF_SECONDARY_UNIT


class StopEvent(object):
    MIN_NUMBER_OF_UNEXPECTED_EVENTS = 5
    MAX_NUMBER_OF_UNEXPECTED_EVENTS = 15
    MIN_DISTANCE_BETWEEN_EVENTS = 10
    MIN_DISTANCE_TO_EDGES = 10

    MIN_BOCK_TIME = 3.0
    MAX_BLOCK_TIME = 10.0

    MIN_NOTICE_DELAY = 5
    MAX_NOTICE_DELAY = 10

    def __init__(self, triggering_time, location_on_track):
        self.triggering_time = triggering_time

        notice_delay = StopEvent.MIN_NOTICE_DELAY + random.random() * (StopEvent.MAX_NOTICE_DELAY - StopEvent.MIN_NOTICE_DELAY)
        block_time = StopEvent.MIN_BOCK_TIME + random.random() * (StopEvent.MAX_BLOCK_TIME - StopEvent.MIN_BOCK_TIME)
        self.block_time_start = triggering_time + notice_delay
        self.block_time_end = self.block_time_start + block_time
        self.location_on_track = location_on_track
        self.is_blocking = False
        self.is_visible = False
        self.is_okay = False
        self.has_passed = False

    def update(self, current_time, loco):
        self.is_blocking = self.block_time_start <= current_time <= self.block_time_end
        self.is_visible = self.triggering_time <= current_time
        self.is_okay = current_time > self.block_time_end
        self.has_passed = loco.traveled_distance > self.location_on_track

    def check_train_crashing(self, loco):
        if self.has_passed:
            return False

        if self.is_blocking:
            position_head = loco.traveled_distance
            # position_tail = position_head - loco.length
            if position_head > self.location_on_track:
                return True
        return False
