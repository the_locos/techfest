#
# simple train visualiser that uses python-cocos2d
# http://python.cocos2d.org
#

from __future__ import division, unicode_literals

import logging

from pyglet.gl import *
from cocos.actions import *
from cocos.director import director
from cocos.layer import Layer, ColorLayer
from cocos.scene import Scene
from cocos.sprite import Sprite

BG_Z_INDEX = -1

ACTOR_Z_INDEX = 0
TRACK_START_POS = (10, 250)
TRACK_END_POS = (600, 250)
ACTOR_IMAGE_SCALE = 0.3

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("TrainVisualiserLogger")


class TrainVisualiser(object):
    def __init__(self, playback):
        logger.info("TrainVisualiser.__init__(): started")
        self.playback = playback

        logger.info("TrainVisualiser.__init__(): done")

    def run(self):
        logger.info("TrainVisualiser.update(): started")

        director.init()
        director.run(Scene(TrainPlayback(self.playback)))

        logger.info("TrainVisualiser.update(); done")


class BackgroundLayer(Layer):
    # enable pyglet's events
    is_event_handler = True

    def __init__(self):
        super(BackgroundLayer, self).__init__()

    def on_enter(self):
        # white background
        self.add(ColorLayer(255, 255, 255, 255), z=BG_Z_INDEX)


class GameLayer(Layer):
    # enable pyglet's events
    is_event_handler = True

    def __init__(self):
        logger.info("GameLayer.__init__(): started")
        super(GameLayer, self).__init__()

        self.image = pyglet.resource.image('train1.png')
        self.image.anchor_x = self.image.width // 2
        self.image.anchor_y = self.image.height // 2
        logger.info("GameLayer.__init__(): done")


class TrainPlayback(GameLayer):
    def __init__(self, playback, init_pos=TRACK_START_POS, target_time=4):
        logger.info("TrainPlayback.__init__(): started")
        super(TrainPlayback, self).__init__()
        # from Reinforcement Learning
        self.playback = playback

        # playback stuff
        self.target_time = target_time
        self.i_step = 0

        # create train object
        self.sprite = Sprite(self.image)
        self.sprite.scale = ACTOR_IMAGE_SCALE
        self.sprite.position = init_pos

        # method to call on every update of frame
        self.schedule(self.play_next_step)

        logger.info("TrainPlayback.__init__(): done")

    def play_next_step(self, a):
        snap = self.playback[self.i_step]
        self.i_step += 1

        # done playback
        if self.i_step >= len(self.playback):
            self.unschedule(self.play_next_step)
            director.pop()

        # TODO: add signal posts

        logger.info("TrainPlayback.on_enter: update on step {}".format(self.i_step))
        target_pos = (TRACK_START_POS[0] + snap * (TRACK_END_POS[0] - TRACK_START_POS[0]), TRACK_START_POS[1])
        self.sprite.do(MoveTo(target_pos))

    def on_enter(self):
        logger.info("TrainPlayback.on_enter(): started")
        super(TrainPlayback, self).on_enter()

        # white background
        self.add(ColorLayer(255, 255, 255, 255), z=BG_Z_INDEX)

        # train
        self.add(self.sprite, z=ACTOR_Z_INDEX)

        logger.info("TrainPlayback.on_enter(): done")


if __name__ == '__main__':
    logger.info("train_visualiser: main started")
    train_visualiser = TrainVisualiser([(i**2 / 10000.0) * 100 for i in range(100)])
    train_visualiser.run()
    logger.info("train_visualiser: main done")
