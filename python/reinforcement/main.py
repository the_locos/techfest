import sys
import argparse

import tensorflow as tf

from environment.loco_env import *
from reinforcement.models import PolicyGradientsNet
#import visualiser


def main(_):
    env = LocoEnv()

    policy = PolicyGradientsNet(env.observation_space.shape[0], n_hidden=16)
    policy.build(learning_rate=FLAGS.learning_rate)
    policy.restore()
    policy.train(env, n_iterations=FLAGS.train_steps, discount_rate=0.95, n_episodes_per_update=10,
                 n_max_steps=FLAGS.max_steps)

    all_episode_rewards = []
    for episode in range(FLAGS.episodes):
        episode_reward = 0
        obs = env.reset()

        for step in range(FLAGS.max_steps):
            action = policy.predict(obs)

            print(np.argmax(action))

            obs, reward, done = env.step(action)
            episode_reward += reward

            if FLAGS.render:
                env.print_status()

            if done:
                env.print_info()
                break

        all_episode_rewards.append(episode_reward)

    if FLAGS.episodes > 1:
        totals = np.asarray(all_episode_rewards)
        print('Min: {}'.format(np.min(totals)))
        print('Max: {}'.format(np.max(totals)))
        print('Mean: {}'.format(np.mean(totals)))
        print('Std: {}'.format(np.std(totals)))
    else:
        print('Reward: {}'.format(all_episode_rewards[0]))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--episodes', type=int, default=1,
                        help='The number of episodes to run')
    parser.add_argument('--learning_rate', type=float, default=0.01,
                        help='The learning rate used')
    parser.add_argument('--max_steps', type=int, default=100000,
                        help='The max number of steps per episode')
    parser.add_argument('--train_steps', type=int, default=1,
                        help='The number of training steps')
    parser.add_argument('--render', type=bool, default=True,
                        help='Set True to render the scene')
    FLAGS, unparsed = parser.parse_known_args()
    tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
