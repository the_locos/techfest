import numpy as np
import tensorflow as tf

from environment.loco_env import LocoEnv


class PolicyGradientsNet(object):
    def __init__(self, n_inputs, n_hidden):
        self.n_inputs = n_inputs
        self.n_hidden = n_hidden
        self.n_outputs = 3  # (-1, 0, 1)

        self.x_ph = None
        self.action = None
        self.y = None
        self.logits = None
        self.gradients = None
        self.gradient_phs = None
        self.train_op = None
        self.sess = tf.InteractiveSession()
        self.saver = None
        pass

    def forward(self):
        initializer = tf.contrib.layers.variance_scaling_initializer()

        self.x_ph = tf.placeholder(tf.float32, shape=[None, self.n_inputs])
        hidden1 = tf.layers.dense(self.x_ph, self.n_hidden, activation=tf.nn.elu,
                                  kernel_initializer=initializer)
        hidden2 = tf.layers.dense(hidden1, self.n_hidden, activation=tf.nn.elu,
                                  kernel_initializer=initializer)
        self.logits = tf.layers.dense(hidden2, self.n_outputs,
                                      kernel_initializer=initializer)
        outputs = tf.nn.softmax(self.logits)
        self.action = tf.multinomial(outputs, num_samples=self.n_outputs)
        self.y = 1.0 - tf.to_float(self.action)

    def build(self, learning_rate):
        self.forward()
        cost = tf.nn.softmax_cross_entropy_with_logits(labels=self.y,
                                                       logits=self.logits)
        optimizer = tf.train.AdamOptimizer(learning_rate)
        grads_and_vars = optimizer.compute_gradients(cost)
        self.gradients = [grad for grad, var in grads_and_vars]
        self.gradient_phs = []
        grads_and_vars_feed = []
        for grad, var in grads_and_vars:
            gradient_ph = tf.placeholder(tf.float32, shape=grad.get_shape())
            self.gradient_phs.append(gradient_ph)
            grads_and_vars_feed.append((gradient_ph, var))
        self.train_op = optimizer.apply_gradients(grads_and_vars_feed)

    def train(self, env: LocoEnv, n_iterations, discount_rate, n_episodes_per_update, n_max_steps,
              save_iterations=50):
        for iteration in range(n_iterations):
            print(iteration)
            all_rewards = []
            all_gradients = []
            for game in range(n_episodes_per_update):
                current_rewards = []
                current_gradients = []

                obs = env.reset()
                for step in range(n_max_steps):
                    action_val, gradients_val = self.sess.run(
                        [self.action, self.gradients],
                        feed_dict={self.x_ph: obs.reshape(1, self.n_inputs)})
                    obs, reward, done = env.step(action_val[0][0])
                    current_rewards.append(reward)
                    current_gradients.append(gradients_val)
                    if done:
                        break
                all_rewards.append(current_rewards)
                all_gradients.append(current_gradients)

            # at this point, we have run the policy 10 episodes, and we are ready for a policy update
            all_rewards = PolicyGradientsNet._discount_and_normalize_rewards(all_rewards, discount_rate)
            feed_dict = {}
            for var_index, grad_ph in enumerate(self.gradient_phs):
                # multiply gradients by the action scores, and compute the mean
                mean_gradients = np.mean(
                    [reward * all_gradients[game_index][step][var_index]
                     for game_index, rewards in enumerate(all_rewards)
                     for step, reward in enumerate(rewards)],
                    axis=0)
                feed_dict[grad_ph] = mean_gradients
            self.sess.run(self.train_op, feed_dict)
            if iteration % save_iterations == 0:
                self.saver.save(self.sess, './tmp/loco_agent.ckpt')

    def restore(self):
        self.saver = tf.train.Saver()
        latest_cp_path = tf.train.latest_checkpoint('./tmp/')
        if latest_cp_path is None:
            self.sess.run(tf.global_variables_initializer())
            print('Could not find latest checkpoint. Use initializer instead.')
        else:
            self.saver.restore(self.sess, latest_cp_path)
            print('Restored checkpoint: ' + latest_cp_path)

    @staticmethod
    def _discount_rewards(rewards, discount_rate):
        discounted_rewards = np.empty(len(rewards))
        cumultative_rewards = 0
        for step in reversed(range(len(rewards))):
            cumultative_rewards = rewards[step] + cumultative_rewards * discount_rate
            discounted_rewards[step] = cumultative_rewards
        return discounted_rewards

    @staticmethod
    def _discount_and_normalize_rewards(all_rewards, discount_rate):
        all_discounted_rewards = [PolicyGradientsNet._discount_rewards(rewards, discount_rate)
                                  for rewards in all_rewards]
        flat_rewards = np.concatenate(all_discounted_rewards)
        rewards_mean = flat_rewards.mean()
        rewards_std = flat_rewards.std()
        return [(discounted_rewards - rewards_mean) / rewards_std
                for discounted_rewards in all_discounted_rewards]

    def predict(self, obs):
        action_val = self.sess.run(
            self.action,
            feed_dict={self.x_ph: obs.reshape(1, self.n_inputs)})
        return action_val[0][0]
